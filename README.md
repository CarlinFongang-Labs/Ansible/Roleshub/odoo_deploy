# Deploy Odoo with Docker-Compose and Ansible Role

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# 1. Ansible Playbook

````bash
mkdir /path/odoo-project
````

````bash
cd /path/odoo-project
````

````bash
nano odoo-deploy.yml
````

## Content
````yaml
---
- name: Setup Odoo
  hosts: localhost
  vars_files:
    - group_vars/all.yml
  roles:
    - odoo_deploy
````

# 2. Requirement

````bash
mkdir /path/odoo-project/roles
````

````bash
nano /path/odoo-project/roles/requirements.yml
````

````yaml
- src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/odoo_deploy.git
  scm: git
  version: "main"
  name: odoo_deploy
````


# 3. Ajust variables

````bash
mkdir /path/odoo-project/group_vars
````

````bash
nano /path/odoo-project/group_vars/all.yml
````


| Variable                   | Valeur par défaut          | Description                                                    |
|----------------------------|----------------------------|----------------------------------------------------------------|
| `odoo_compose_directory`   | `.`                        | Chemin du répertoire contenant le fichier Docker Compose       |
| `odoo_compose_file`        | `odoo-template.yml.j2`     | Fichier template Docker Compose                                |
| `ODOO_PASSWORD_FILE`       | `./.secrets/odoo_pg_pass`  | Emplacement du fichier contenant le mot de passe Odoo          |
| `ODOO_IMAGE`               | `odoo:16.0`                | Image Docker pour le service Odoo                              |
| `ODOO_EXPOSE_PORT`         | `8093`                     | Port exposé pour le service Odoo                               |
| `ODOO_NETWORK`             | `odoo_network`             | Réseau Docker utilisé par les services                         |
| `POSTGRES_IMAGE`           | `postgres:15`              | Image Docker pour le service PostgreSQL                        |
| `POSTGRES_DB`              | `postgres`                 | Nom de la base de données PostgreSQL                           |
| `POSTGRES_USER`            | `odoo`                     | Utilisateur de la base de données PostgreSQL                   |
| `ODOO_WEB_DATA_VOLUME`     | `odoo-web-data`            | Volume pour les données web Odoo                               |
| `ODOO_CONFIG_VOLUME`       | `./config`                 | Volume de stockage des fichiers de configuration Odoo          |
| `ODOO_ADDONS_VOLUME`       | `./addons`                 | Volume pour les addons (plugins) Odoo                          |
| `POSTGRES_DB_DATA_VOLUME`  | `odoo-db-data`             | Volume pour les données de la base de données PostgreSQL       |
| `ODOO_SERVICE_NAME`        | `odoo_web_service`         | Nom du service Web Odoo dans Docker Compose                    |





# 4. Launch

````bash
cd /path/odoo-project
````

````bash
ansible-galaxy install -r roles/requirements.yml -f
````

````bash
ansible-playbook odoo-deploy.yml -vvv
````

🥳🎉🥳 : Félicitation, Odoo est déployé sur votre serveur


